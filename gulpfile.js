var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var wiredep = require('wiredep').stream;
var mainBowerFiles = require('main-bower-files');
var concat = require('gulp-concat');
var inject = require('gulp-inject');
var filter = require('gulp-filter');

var OPTIONS = {
  srcPath: "src",
  buildPath: "public"
}

gulp.task('serve', [ 'sass', 'scripts', 'inject' ], function () {
  browserSync.init({
    server: {
      baseDir: "./public"
    }
  });

  gulp.watch(OPTIONS.srcPath + "/*.ejs", [ 'inject' ]);
  gulp.watch(OPTIONS.srcPath + "/scss/*.scss", [ 'sass' ]);
  gulp.watch(OPTIONS.srcPath + "/js/*.js", [ 'scripts' ]);
  gulp.watch(OPTIONS.srcPath + "/*.html").on('change', browserSync.reload);
});

gulp.task('sass', function () {
  return gulp.src(OPTIONS.srcPath + "/scss/*.scss")
    .pipe(sass())
    .pipe(gulp.dest(OPTIONS.buildPath + "/css"))
    .pipe(browserSync.stream());
});

gulp.task('scripts', function () {
  return gulp.src(OPTIONS.srcPath + "/js/*.js")
    .pipe(gulp.dest(OPTIONS.buildPath + "/js"))
    .pipe(browserSync.stream());
});

gulp.task('bower', function () {
  var jsFilter = filter('**/*.js', {restore: true});
  var cssFilter = filter('**/*.css', {restore: true});

  return gulp.src(mainBowerFiles())
    .pipe(jsFilter)
    .pipe(concat('vendors.js'))
    .pipe(gulp.dest(OPTIONS.buildPath + '/js'))
    .pipe(jsFilter.restore)
    .pipe(cssFilter)
    .pipe(concat('vendors.css'))
    .pipe(gulp.dest(OPTIONS.buildPath + '/css'))
    .pipe(cssFilter.restore)
});

gulp.task('inject', ['bower'], function () {
  var bowerScripts = gulp.src([OPTIONS.buildPath + '/js/vendors.js', OPTIONS.buildPath + '/css/vendors.css'], {read: false});
  var sources = gulp.src([OPTIONS.buildPath + '/js/*.js', '!' + OPTIONS.buildPath + '/js/vendors.js', '!' + OPTIONS.buildPath + '/css/vendors.css', OPTIONS.buildPath + '/css/**/*.css'], {read: false});

  gulp.src(OPTIONS.srcPath + '/*.ejs')
    .pipe(gulp.dest(OPTIONS.buildPath))
    .pipe(inject(bowerScripts, {relative: true, name: 'bower'}))
    .pipe(inject(sources, {relative: true}))
    .pipe(gulp.dest(OPTIONS.buildPath));
});

gulp.task('default', ['serve']);