'use strict';

var express = require('express');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var restify = require('express-restify-mongoose');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var router = express.Router();

app.use(bodyParser.json());
app.use(methodOverride());

/************************
 * Mongoose Modeling
 */

mongoose.connect('mongodb://htp2016:k4B*G`=G}.L#k2Y7@ds041556.mlab.com:41556/hoister-to-presidency');

/********
 * Party Model
 */

restify.serve(router, mongoose.model('Party', new mongoose.Schema({
  name: { type: String, required: true },
  leader: { type: String, required: true },
  score: { type: Number }
})) /*, {
    preUpdate: function (req, res, next) {
     console.log(req.erm.model);
     console.log(req.body.score);
     next()
    }
    }*/);

/*******
 * Trophy Model
 */

restify.serve(router, mongoose.model('Trophy', new mongoose.Schema({
  name: { type: String, required: true },
  description: { type: String },
  score: { type: Number }
})));

/*******
 * User Model
 */

var User = mongoose.model('User', new mongoose.Schema({
  id: { type: String, required: true },
  name: { type: String, required: true },
  username: { type: String, required: true },
  party: { type: Number, default: null },
  scored: { type: Number, default: 0 },
  messages: [{ type: Schema.Types.ObjectId, ref: 'Message' }]
}))

restify.serve(router, User);

/*******
 * Message Model
 */

restify.serve(router, mongoose.model('Message', new mongoose.Schema({
  name: { type: String, required: true },
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  score: { type: Number }
})));

/*********************
 * Sockets
 */

io.on('connection', function (socket) {
  socket.emit('connected');
  socket.userModel = new User();

  // Set User

  socket.on('setUser', function(user) {
    User.where({ id: user.userId }).findOne(function(err, dbUser) {
      if (err) return handleError(err);

      if (dbUser !== null) {
        socket.userModel = dbUser;
      } else {
        socket.userModel = new User({
          id: user.userId,
          name: user.name,
          username: user.userName
        });

        socket.userModel.save();
      }
    })
  });

  // Set Party

  socket.on('setParty', function(partyId) {
    socket.userModel.party = partyId;
    socket.userModel.save(function (err, dbUser) {
      if (err) return console.error(err);
    });
  });

  // Hoist

  socket.on('hoist', function (meters) {
    socket.userModel.scored = (socket.userModel.scored + meters).toFixed(2);
    socket.userModel.save(function (err, dbUser) {
      if (err) return console.error(err);
    });
  });

  //

});


/*********************
 * App routing
 */

app.use(router);
app.use(express.static('public'));

app.engine('.html', require('ejs').__express);
app.set('views', __dirname + '/public');
app.set('view engine', 'ejs');

app.get('/', function(req, res) {
  // res.sendFile(path.join(__dirname + '/public/index.html'));
  User.find({}, function(err, users) {
    var clintonScore = 0;
    var trumpScore = 0;

    users.forEach(function(user) {
      if(user.party == 1) {
        clintonScore = parseFloat(+clintonScore + parseFloat(user.scored));
      } else if(user.party == 2) {
        trumpScore = parseFloat(+trumpScore + parseFloat(user.scored));
      }
    });

    res.render('index', {
      title: "The Hoist, 2016 Presidency",
      clintonScore: parseInt(clintonScore),
      trumpScore: parseInt(trumpScore)
    });
  });


});

/*********************
 * Launch app
 */

server.listen(process.env.PORT || 5000, function () {
  console.log('Express server listening on port 5000');
});
