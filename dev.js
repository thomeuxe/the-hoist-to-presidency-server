const express = require('express')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const restify = require('express-restify-mongoose')
const app = express()
const server = require('http').Server(app);
const io = require('socket.io')(server);
const router = express.Router()

app.use(bodyParser.json())
app.use(methodOverride())

/************************
 * Mongoose Modeling
 */

mongoose.connect('mongodb://htp2016:k4B*G`=G}.L#k2Y7@ds041556.mlab.com:41556/hoister-to-presidency')

/********
 * Party Model
 */

restify.serve(router, mongoose.model('Party', new mongoose.Schema({
  name: { type: String, required: true },
  leader: { type: String, required: true },
  score: { type: Number }
}))/*, {
  preUpdate: function (req, res, next) {
    console.log(req.erm.model);
    console.log(req.body.score);
    next()
  }
}*/)

/*******
 * Trophy Model
 */

restify.serve(router, mongoose.model('Trophy', new mongoose.Schema({
  name: { type: String, required: true },
  description: { type: String },
  score: { type: Number }
})))

/*******
 * User Model
 */

restify.serve(router, mongoose.model('User', new mongoose.Schema({
  name: { type: String, required: true },
  fid: { type: String, required: true },
  party: { type: Schema.Types.ObjectId, ref: 'Party' },
  scored: { type: Number },
  currentScoring: { type: Number },
  messages : [{ type: Schema.Types.ObjectId, ref: 'Message' }]
})))

/*******
 * Message Model
 */

restify.serve(router, mongoose.model('Message', new mongoose.Schema({
  name: { type: String, required: true },
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  score: { type: Number },
})))

/*********************
 * Sockets
 */


io.on('connection', function (socket) {
  socket.emit('connected');
  socket.on('hoist', function (data) {
    console.log(data);
  });
});


/*********************
 * Launch app
 */

app.use(router)

app.listen(process.env.PORT || 5000, () => {
  console.log('Express server listening on port 5000')
})